import 'package:flutter/material.dart';
import 'package:lab2_stream/stream/stream_word.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  int count = 0;
  StreamWord streamWord = StreamWord();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Stream New Words",
      home: Scaffold(
        appBar: AppBar(title: Text('App Bar'),),
        body: Text('How many new words? $count', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
        floatingActionButton: FloatingActionButton(
          child: Center(
            child: Text("Show Answer"),
          ),
          onPressed: () {
            showNumberWord();
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }

  showNumberWord() async{
    streamWord.getWords().listen((event) {
      setState(() {
        print(event);
        count = event;
      });
    });
  }
}
